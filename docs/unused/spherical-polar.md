# Spherical-polar coordinates

## Geographic latitude, longitude, altitude
Geographic coordinates are earth-centered earth-fixed (ECEF) in modified spherical polar form. The angles are longitude (the cylindrical angle) and latitude (the polar angle). The altitude is the distance from the surface of the earth, not the radial distance from the center of the earth, which is what makes these coordinates "modified" spherical polar.

* `latlonalt_deg(latitude_deg, longitude_deg, altitude_m)` will create an `lla` structure  that the frame conversion functions can read.
* For example, `umd_lla = latlonalt_deg(38.988933, -76.937115, 42)` defines a geographic location in College Park, Maryland.

## Topocentric azimuth, elevation, range
Topocentric coordinates are centered at a location on or near the surface of the earth and are oriented with the principal (1) axis pointing east and the polar (3) axis pointing up, or radially outward from the center of the earth. Tradition dictates that azimuth, or compass angle, is measured clockwise from north rather then counter-clockwise from east as mathematical convention, so these coordinates are a modification of the standard spherical-polar convention.

* `azelrn_deg (azimuth_deg, elevation_deg, range_m)` will create a structure from the values in degrees for azimuth and elevation and range in meters that the frame conversion functions can read.
* All frame conversion functions that take or return an `azelrn` structure will require a geographic location `lla` structure to identify the origin.

## Right ascension and declination
The spherical polar coordinates for the earth-centered inertial (ECI) reference frames are _right ascension_ (the cylindrical, or "longitude" angle), and _declination_ (the polar, or "latitude angle). The right ascension is the angle (positive = counter-clockwise) between the vernal equinox and the projection of the vector in the equatorial plane. The declination is the angle between the equator and the vector to the point, measured positive if in the northern hemisphere. The function [`radec()`](ephemeris.md#location-conversion) will compute right ascension and declination, as well as geocentric distance, from Cartesian postions.
